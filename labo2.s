.global main

N = 4  // Nombre de lignes/colonnes de la matrice
K = 8  // Nombre d'octets d'un élément de la matrice

// Programme qui détermine si une matrice N × N est un carré quasi-magique
// x19 -- l
// x20 -- a
// x21 -- b
// x22 -- i (index colonne)
// x23 -- lineSum
// x24 -- columnSum

main:
        // Lire carré                   //
        mov     x19, 0                  // l = 0
        adr     x20, carre              // a = &carre
read:                               	// do {
        adr     x0, fmtVal              //
        mov     x1, x20                 //
        bl      scanf                   //   scanf(&fmtVal, a)
        add     x19, x19, 1             //   l += 1
        add     x20, x20, K             //   a += K
        cmp     x19, N*N                // }
        b.lo    read                	// while (l < N*N)

// Somme de la premiere ligne
lineSumInit:
		mov 	x19, 0					// l = 0
		mov		x23, 0					// lineSum = 0
		adr		x20,carre				// a = &carre
lineSum:								// do {
		ldrb 	w21, [x20]				//   lire 8 bytes de a dans b
		add		x23, x23, x21			//	 lineSum += b
        add     x19, x19, 1             //   l += 1
        add     x20, x20, K             //   a += K
        cmp     x19, N					// }
		b.lo 	lineSum					// while (l < N)

		mov 	x19, 0					// l = 0
		mov		x22, 1					// i = 0
		mov		x24, 0					// columnSum = 0
		adr		x20,carre				// a = &carre
columnSum:								// do {
		ldrb 	w21, [x20]				//   lire 8 bytes de a dans b
		add		x24, x24, x21			//	 lineSum += b
		add     x19, x19, 1             //   l++
		add     x20, x20, K*N           //   a += K*N
		cmp     x19, N					// }
		b.lo 	columnSum				// while (l < N)

		cmp		x23, x24				//
		b.eq	nextColumn				// branche si lineSum == columnSum
		b		isInvalid

nextColumn:
		cmp		x22, N
		b.eq	isValid					// branche si i == N
		mov 	x19, 0					// l = 0
		mov		x24, 0					// columnSum = 0
		add		x22, x22, 1				// i++
		adr		x20,carre				// a = &carre
		mov		x26, K
		mul		x26, x26, x22
		add		x20, x20, x26
		b		columnSum

isValid:
		adr		x0,msgValide			//Param1: adresse du message
		bl		printf
		b		end

isInvalid:
		adr		x0,msgInvalide			//Param1: adresse du message
		bl		printf

end:
        bl      exit                    // quitter le programme

.section ".bss"
                .align  K
carre:          .skip   N*N*K

.section ".rodata"
xd:				.asciz 	"%d\n"
fmtVal:         .asciz  "%lu"
msgValide:      .asciz  "valide\n"
msgInvalide:    .asciz  "invalide\n"
